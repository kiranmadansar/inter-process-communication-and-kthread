/********************************************************************************************************
*
* @file queue1.c
* @brief inter process communication using pipes
*
* This is the example program for inter process communication using message queues
*
* @author Kiran Hegde
* @date 3/5/2018
* @tools vim editor
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/
#include <stdio.h>
#include <semaphore.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <mqueue.h>
#include "ip_comm.h"
#include <unistd.h>
#include <errno.h>


/********************************************************************************************************
*
* @name main
* @brief main function
*
* This function creates a message queue and waits from data from other process
* once received, it sends back some data
*
* @param None
*
* @return zero on successful execution, otherwise error code
*
********************************************************************************************************/
int main()
{
	mqd_t process1;
	struct mq_attr attr;
	/* attribute from message queue*/
	attr.mq_maxmsg = 5;
	attr.mq_msgsize = sizeof(struct message);
	sem_t *lock;
	/* open semaphore */
	lock = sem_open("sem", O_CREAT | O_RDWR, 0666, 0);
	struct message msg_proc1;
	char str[BUF_SIZE]="Process1";
	/* open a message queue */
	if((process1 = mq_open("/proc1", O_RDWR | O_CREAT, 0666, &attr))==-1)
	{
		printf("Error opening message queue\n");
		exit(1);
	}

	/* wait from semaphore */
	sem_wait(lock);
	/* receive from message queue */
	if((mq_receive(process1, &msg_proc1, sizeof(struct message), 0))==-1)
	{
		printf("Din't receive message from process 2 and returned %d\n", errno);
	}
	
	printf("Length = %d\n", msg_proc1.len);
	printf("PID of sender = %d\n", msg_proc1.pid);
	printf("String = %s\n", msg_proc1.mystring);
	printf("LED = %d\n", msg_proc1.LED);
#ifdef BBG
        if((LEDHandle = fopen(LEDBrightness, "r+")) != NULL){
        fwrite(msg_proc1.LED, sizeof(char), 1, LEDHandle);
        fclose(LEDHandle);
#endif

	
	msg_proc1.LED=1;
        msg_proc1.len=strlen(str);
        msg_proc1.pid=getpid();
        memcpy(msg_proc1.mystring, str, BUF_SIZE);
	/*send to message queue */
        if((mq_send(process1, &msg_proc1, sizeof(struct message),0))==-1)
        {
                printf("cant send message to process1\n");
        }
	sem_post(lock);

	/* close the queue and unlink it*/
	mq_close(process1);
	
	mq_unlink("/proc1");
	return 0;
}
