/********************************************************************************************************
*
* @file ip_comm.h
* @brief library file common for all ipc programs 
*
* This file defines the message structure that is used to do IPC communication
*
* @author Kiran Hegde
* @date 3/4/2018
* @tools vim editor
*
********************************************************************************************************/

#ifndef IP_COMM_H_

#define IP_COMM_H_

/********************************************************************************************************
*
* Header files
*
********************************************************************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>


/********************************************************************************************************
*
* Macros definition
*
********************************************************************************************************/

#define BUF_SIZE 20


/********************************************************************************************************
*
* @name structure message
* @brief contains buffer, length, LED status and pid
*
* This structure defines a buffer for storing string and contains buffer length, LED status
* and process identifier field
*
* @param None
*
* @return None
*
********************************************************************************************************/
struct message
{
	uint8_t len;
	char mystring[20];
	bool LED;
	unsigned int pid;
};

/* for controlling beagle bone LED*/
#ifdef BBG
FILE *LEDHandle = NULL;
char *LEDBrightness = "/sys/class/leds/beaglebone:green:usr0/brightness";
#endif
#endif
