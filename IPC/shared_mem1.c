/********************************************************************************************************
*
* @file shared_mem1.c
* @brief inter process communication using shared memory
*
* This is the example program for inter process communication using shared memory
* semaphores are used for synchronisation
*
* @author Kiran Hegde
* @date 3/4/2018
* @tools vim editor
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <fcntl.h>
#include "ip_comm.h"
#include <sys/ipc.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>


/********************************************************************************************************
*
* @name main
* @brief main function
*
* This function creates shared memory and maps for this address space, 
* It waits fir other process to send data, once it is done, this process sends some data back
*
* @param None
*
* @return zero on successful execution, otherwise error code
*
********************************************************************************************************/
int main()
{
        int result, shmid;
        sem_t *lock;
        char buffer[BUF_SIZE]="Process2";
	/* create a semaphore lock */
	lock = sem_open("sem", O_CREAT, 0666, 0);
        struct message message1;
	struct message *temp;

	/* create a shared memory area */
        if((shmid = shm_open("/share", O_RDWR | O_CREAT, 0666)) == -1)
        {
                printf("Shared memory error returned %d\n",errno);
                exit(1);
        }

	/* map the memory created with this process address space*/
        if((temp = mmap(NULL, sizeof(struct message), PROT_READ | PROT_WRITE, MAP_SHARED, shmid, 0)) == (struct message *)-1)
        {
                printf("Attaching memory failed %d\n", errno);
                exit(1);
        }

	/* WAIT FOR SEMAPHORE */
	sem_wait(lock);
	printf("Length = %d\n", temp->len);
	printf("My String = %s\n", temp->mystring);
	printf("LED = %d\n", temp->LED);
	printf("PID of sender = %d\n", temp->pid);	

#ifdef BBG
	if((LEDHandle = fopen(LEDBrightness, "r+")) != NULL){
      	fwrite(temp->LED, sizeof(char), 1, LEDHandle);
      	fclose(LEDHandle);
#endif
	/* assign new values to communicate it back to other process*/
	message1.LED=0;
	message1.len = strlen(buffer);
	message1.pid = getpid();
	memcpy(message1.mystring, buffer, BUF_SIZE);
	/* put data into shared memory */
	*temp = message1;
	/* give back the semaphore */
	sem_post(lock);
	close(shmid);
	/* unlink the sharedmemory from this address space*/
	shm_unlink("/share");
 	return 0;
}
