/********************************************************************************************************
*
* @file socket1.c
* @brief inter process communication using sockets
*
* This is the example program for inter process communication using message sockets
* This is the server 
*
* @author Kiran Hegde
* @date 3/5/2018
* @tools vim editor
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "ip_comm.h"

#include <netinet/in.h>

/* define port */
#define PORT 5000


/********************************************************************************************************
*
* @name main
* @brief main function
*
* Opens a socket and binds it to port. Listens on port 5000 for communication.
* accepts the request, reads and data and send back some information
*
* @param None
*
* @return zero on successful execution, otherwise error code
*
********************************************************************************************************/
int main()
{

	int server, sock, read_sock;
	struct sockaddr_in address;
	int len = sizeof(address);
	struct message msg;
	int option=1;
	char str[BUF_SIZE]="Server";

	/* create a socket */
	if((server = socket(AF_INET, SOCK_STREAM, 0))==0)
	{
		printf("server creation failed\n");
		exit(0);
	} 

	/* set socket options */
	if(setsockopt(server, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &option, sizeof(option)))
	{
		printf("Can't set socket \n");
		exit(0);
	}

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(PORT);
	
	/* bind the socket to port mentioned */
	if((bind(server, (struct sockaddr *)&address, sizeof(address)))<0)
	{
		printf("Port binding failed\n");
		exit(0);
	}
	
	/* listen for connection */
	if(listen(server, 3) < 0)
	{
		printf("Can't listen\n");
	}

	/* accept the connection from client */
	sock = accept(server, (struct sockaddr *)&address, (socklen_t *)&len);
	if(sock < 0)
	{
		printf("Can't accept connection\n");
	}
	
	/* read data from client */
	read_sock = read(sock, &msg, sizeof(msg));
	printf("Length = %d\n", msg.len);
	printf("LED = %d\n", msg.LED);
	printf("String = %s\n", msg.mystring);
	printf("PID of sender = %d\n", msg.pid);
#ifdef BBG
        if((LEDHandle = fopen(LEDBrightness, "r+")) != NULL){
        fwrite(msg.LED, sizeof(char), 1, LEDHandle);
        fclose(LEDHandle);
#endif

	msg.len = strlen(str);
        msg.LED = 1;
        msg.pid = getpid();
        memcpy(msg.mystring, str, BUF_SIZE);
	/* send data into socket */
	send(sock, &msg, sizeof(struct message), 0);

	/* close the server socket*/
	close(server);
	return 0;
}
