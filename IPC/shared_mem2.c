/********************************************************************************************************
*
* @file shared_mem2.c
* @brief inter process communication using shared memory
*
* This is the example program for inter process communication using shared memory
* semaphores are used for synchronisation
*
* @author Kiran Hegde
* @date 3/5/2018
* @tools vim editor
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <fcntl.h>
#include "ip_comm.h"
#include <sys/ipc.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>


/********************************************************************************************************
*
* @name main
* @brief main function
*
* This function creates shared memory and maps for this address space, 
* It waits fir other process to send data, once it is done, this process sends some data back
*
* @param None
*
* @return zero on successful execution, otherwise error code
*
********************************************************************************************************/
int main()
{
	int result, shmid;
	sem_t *lock;
	/* open the semaphore, if exists, or create one*/
	lock = sem_open("sem", O_CREAT | O_RDWR, 0644, 0);
	struct message *shm, message2;
	char string[BUF_SIZE]="Process1";

	/* create a shared memory area */
	if((shmid = shm_open("/share", O_RDWR | O_CREAT, 0666)) == -1)
	{
		printf("Shared memory error returned %d\n",errno);
		exit(1);
	}
 
	result = ftruncate(shmid, sizeof(struct message));	

	/* map the memory created with this process address space*/
	if((shm = mmap(NULL, sizeof(struct message), PROT_READ | PROT_WRITE, MAP_SHARED, shmid, 0)) == (struct message *)-1)
	{
		printf("Attaching memory failed %d\n", errno);
		exit(1);
	}

	memcpy(message2.mystring, string, BUF_SIZE);
	message2.len=strlen(string);
	message2.LED = 1;
	message2.pid = getpid();

	/* put data into shared memory */
	*shm = message2;
	/* give semaphore */
	sem_post(lock);
	usleep(50);
	sem_wait(lock);
	printf("Length = %d\n", shm->len);
        printf("My String = %s\n", shm->mystring);
        printf("LED = %d\n", shm->LED);
	printf("PID of sender = %d\n", shm->pid);
	/* close and delete semaphore */
#ifdef BBG
        if((LEDHandle = fopen(LEDBrightness, "r+")) != NULL){
        fwrite(shm->LED, sizeof(char), 1, LEDHandle);
        fclose(LEDHandle);
#endif

	sem_close(lock);
	sem_destroy(lock);

	close(shmid);

	/* unlink the sharedmemory from this address space*/
	shm_unlink("/share");
	return 0;
}
