/********************************************************************************************************
*
* @file socket2.c
* @brief inter process communication using sockets
*
* This is the example program for inter process communication using message sockets
* This is the Client
*
* @author Kiran Hegde
* @date 3/5/2018
* @tools vim editor
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/

#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "ip_comm.h"
#include <arpa/inet.h>

#include <netinet/in.h>

/* define port number */
#define PORT 5000

/********************************************************************************************************
*
* @name main
* @brief main function
*
* Opens a socket and binds it to port. connects to server and sends data.
* log the incoming data from server
*
* @param None
*
* @return zero on successful execution, otherwise error code
*
********************************************************************************************************/

int main()
{

        int client, sock, read_sock;
        struct sockaddr_in address;
        int len = sizeof(address);
        struct message msg;
	char str[BUF_SIZE] = "Client";
	msg.len = strlen(str);
	msg.LED = 0;
	msg.pid = getpid();
	memcpy(msg.mystring, str, BUF_SIZE);

        int option=1;
	/* open socket */
        if((client = socket(AF_INET, SOCK_STREAM, 0))<0)
        {
                printf("server creation failed\n");
                exit(0);
        }
	
	address.sin_family = AF_INET;
	address.sin_port = htons(PORT);
	
	/* converts IP address to proper format */
	if((inet_pton(AF_INET, "127.0.0.1", &address.sin_addr))<=0)
	{
		printf("Addr error\n");
	}
	
	/* connect to the server */
	if((connect(client, (struct sockaddr *)&address, sizeof(address)))<0)
	{
		printf("Can't connect to server\n");
	}

	/* send the data to socket */
	send(client, &msg, sizeof(struct message), 0);
	
	/* read from socket */
	read(client, &msg, sizeof(msg));
        printf("Length = %d\n", msg.len);
        printf("LED = %d\n", msg.LED);
        printf("String = %s\n", msg.mystring);
        printf("PID of sender = %d\n", msg.pid);
#ifdef BBG
        if((LEDHandle = fopen(LEDBrightness, "r+")) != NULL){
        fwrite(msg.LED, sizeof(char), 1, LEDHandle);
        fclose(LEDHandle);
#endif
	/* close the socket */
	close(client);
	return 0;
}
