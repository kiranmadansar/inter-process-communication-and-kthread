/********************************************************************************************************
*
* @file pipes.c
* @brief inter process communication using pipes
*
* This is the example program for inter process communication using pipes
*
* @author Kiran Hegde
* @date 3/4/2018
* @tools vim editor
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "ip_comm.h"
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

/********************************************************************************************************
*
* @name main
* @brief main function
*
* This function forks another process and using pipes some informations are exchanged
*
* @param None
*
* @return zero on successful execution, otherwise error code
*
********************************************************************************************************/

int main()
{
	uint8_t result;
	pid_t p;

	/* declare two pipes */
	int pipe_1[2], pipe_2[2];
		
	/* open pipe 1*/
	if((pipe(pipe_1))==-1)
	{
		printf("Opening pipe failed\n");
		return 1;
	}

	/* pipe 2*/
	if((pipe(pipe_2))==-1)
	{
		printf("Opening 2nd pipe failed\n");
		return 1;
	}
	
	/* create another process*/
	p = fork();
	if(p<0)
	{
		printf("Fork failed\n");
		return 1;
	}
	
	else if(p>0)
	{
		struct message msg;
		char string[BUF_SIZE]="Parent";
        	memcpy(msg.mystring, string, BUF_SIZE);
       	 	msg.len=strlen(string);
        	msg.LED=0;
        	msg.pid = getpid();

		/* close the reading end */
		close(pipe_1[0]);
		/* write to pipe */
		write(pipe_1[1], &msg, sizeof(struct message));
		/* close the writing end */
		close(pipe_1[1]);
		
		usleep(500);
		
		/* close writing end*/
		close(pipe_2[1]);
		/* read from pipe */
		read(pipe_2[0], &msg, sizeof(struct message));
		printf("Length from parent= %d\n", msg.len);
                printf("String from parent= %s\n", msg.mystring);
                printf("LED status from parent= %d\n", msg.LED);
                printf("PID of parent = %d\n", msg.pid);	
		close(pipe_2[0]);
#ifdef BBG
        if((LEDHandle = fopen(LEDBrightness, "r+")) != NULL){
        fwrite(msg.LED, sizeof(char), 1, LEDHandle);
        fclose(LEDHandle);
#endif

		
	}
	
	else
	{
		char str[BUF_SIZE]="Child";
		struct message msg_child;
		usleep(20);
		/* close writing end */
		close(pipe_1[1]);
		/* read from pipe */
		read(pipe_1[0], &msg_child, sizeof(struct message));
		printf("Length from child= %d\n", msg_child.len);
		printf("String from child= %s\n", msg_child.mystring);
		printf("LED status from child= %d\n", msg_child.LED);
		printf("PID of child = %d\n", msg_child.pid);

#ifdef BBG
        if((LEDHandle = fopen(LEDBrightness, "r+")) != NULL){
        fwrite(msg_child.LED, sizeof(char), 1, LEDHandle);
        fclose(LEDHandle);
#endif

		msg_child.LED=1;
		msg_child.len = strlen(str);
		memcpy(msg_child.mystring, str, BUF_SIZE);
		msg_child.pid = getpid();
		
		/* close both pipe for reading */
		close(pipe_1[0]);
		close(pipe_2[0]);
		
		/* write to pipe */
		write(pipe_2[1], &msg_child, sizeof(struct message));
		close(pipe_2[1]);
		exit(0);
	}
	return 0;
}
