/*******************************************************************************************************
*
* UNIVERSITY OF COLORADO BOULDER
*
* @file traverse_process.c
* @brief traverses parent processes
* 
* This kernel module traverses from current process to init process through parent processes
*
* @author Kiran Hegde
* @date  3/3/2018
* @tools vim editor
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/init.h>

/********************************************************************************************************
*
* Module description, licence and author information
*
********************************************************************************************************/

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Traversing the process tree through parent process");
MODULE_AUTHOR("KIRAN-HEGDE");

/********************************************************************************************************
*
* @name kmodule_entry()
* @brief initialisation for module
* 
* Once installed, kernel module starts executing from this function. 
* This module traverses the process from current to init process and logs
* PID, status, Nice value, Number of children, thread name to kernel logger 
*
* @param None
* @return return 0 on successful execution 
*
********************************************************************************************************/
static int __init kmodule_entry(void)
{
	printk(KERN_INFO "STARTING......\n");
	struct list_head *ptr;
	int count=0, nice_val=50;
	struct task_struct * tsk = current;
	if(!tsk) return 1;
	for(tsk = current; tsk->pid!=1; tsk=tsk->parent)
	{
		printk(KERN_INFO "PID is %d\n", tsk->pid);
		printk(KERN_INFO "Process Status is %li\n", tsk->state);
		/* linked list traverse for every child process */
		list_for_each(ptr, &(tsk->children))
		{
			++count;	
		}
		/* function to find out nice value */
		nice_val = task_nice(tsk);
		printk(KERN_INFO "Nice Value is %d\n", nice_val);
		printk(KERN_INFO "Number of children is %d\n", count);
		printk(KERN_INFO "Thread Name is %s\n", (tsk->comm));
		printk(KERN_INFO "\n");
	}
	printk(KERN_INFO "PID %i\n", tsk->pid);
	printk(KERN_INFO "Process Status is %li\n", tsk->state);
        list_for_each(ptr, &(tsk->children))
        {
 	       ++count;
        }
	nice_val = task_nice(tsk);
        printk(KERN_INFO "Nice Value is %d\n", nice_val);
        printk(KERN_INFO "Number of children is %d\n", count);
        printk(KERN_INFO "Thread Name is %s\n", (tsk->comm));
        printk(KERN_INFO "\n");
	return 0;
}

/********************************************************************************************************
*
* @name timer_module_exit()
* @brief remove/delete the timer
* 
* This function executes when the kernel module is removed.
* It removes the kernel module
*
* @param None
* @return None
*
********************************************************************************************************/
static void __exit kmodule_exit(void)
{
	printk(KERN_INFO "EXITING....\n");
}

/*Informing the Kernel about module entry function and
module exit function*/
module_init(kmodule_entry);
module_exit(kmodule_exit);
