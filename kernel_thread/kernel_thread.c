/*******************************************************************************************************
*
* UNIVERSITY OF COLORADO BOULDER
*
* @file traverse_process.c
* @brief kfifo communication
* 
* This kernel module creates two threads and communicates using kfifo
* Thread1 computes the PID and vruntime of next, current, previous tasks
* thread2 logs the above mentioned parameters
*
* @author Kiran Hegde
* @date  3/3/2018
* @tools vim editor
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/

#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/kfifo.h>
#include <linux/list.h>

/********************************************************************************************************
*
* Module description, licence and author information
*
********************************************************************************************************/
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("kthread practice to create two threads");
MODULE_AUTHOR("KIRAN-HEGDE");

/* Global declaration */
static struct task_struct *thread1=NULL, *thread2=NULL;
struct kfifo k_fifo;
uint8_t result;
struct task_struct buf_out;

/********************************************************************************************************
*
* @name threadfunc1
* @brief allocate kfifo and put current task into kfifo
*
* This function allocates kfifo and puts the current task's task_struct into kfifo
* this is repeated for every 5 second
*
* @param unused pointer
*
* @return zero on succesful execution or error
*
********************************************************************************************************/
static int threadfunc1(void *un)
{
	result = kfifo_alloc(&k_fifo, 5*sizeof(struct task_struct), GFP_KERNEL);
	if(result) return result;
	struct task_struct *tsk = current; 
	if(!tsk) return -1;
	while(!kthread_should_stop())
	{
		result = kfifo_in(&k_fifo, tsk, sizeof(struct task_struct));
		if(!result) return result;
		ssleep(5);
	}
	do_exit(0);
	return 0;
}

/********************************************************************************************************
*
* @name threadfunc2
* @brief logger
*
* This function gets the PID and vruntime of the current, previous and next task
* Logs it tp kenrel logger 
* repeats at every 5 seconds
*
* @param unused pointer
*
* @return zero on successful execution, othrwise error 
*
********************************************************************************************************/

static int threadfunc2(void *un)
{
	while(!kthread_should_stop())
	{
		struct task_struct *temp;
		result = kfifo_out(&k_fifo, &buf_out, sizeof(struct task_struct));
		temp = &buf_out;
	 	if(temp)
		{
			printk(KERN_INFO "Previous: PID = %lu \t Vruntime = %llu\n", \
							list_prev_entry(temp, tasks)->pid, \
							list_prev_entry(temp, tasks)->se.vruntime);
			printk(KERN_INFO "Current : PID = %lu \t Vruntime = %llu\n", \
							temp->pid, temp->se.vruntime);
			printk(KERN_INFO "Next    : PID = %lu \t Vruntime = %llu\n\n", \
							list_next_entry(temp, tasks)->pid, \
                        				list_next_entry(temp, tasks)->se.vruntime);
		}
		ssleep(5);
	}
	do_exit(0);
	return 0;
}

/********************************************************************************************************
*
* @name module_start()
* @brief initialisation for module
* 
* Once installed, kernel module starts executing from this function.
* Creates two threads 
*
* @param None
* @return return 0 on successful execution 
*
********************************************************************************************************/
static int __init module_start(void)
{
	printk(KERN_INFO "Starting kernel module....\n\n");
	thread1 = kthread_run((threadfunc1), NULL, "thread_1");
	thread2 = kthread_run((threadfunc2), NULL, "thread_2");	
	return 0;
}


/********************************************************************************************************
*
* @name module_stop()
* @brief thread stop
* 
* Stops the thread from executing, if exists.
*
* @param None
* @return None
*
********************************************************************************************************/
static void __exit module_stop(void)
{
	if(thread1) kthread_stop(thread1);
	if(thread2) kthread_stop(thread2);
	kfifo_free(&k_fifo);
	printk(KERN_INFO "\nEXITED....\n");	
}

/*Informing the Kernel about module entry function and
module exit function*/
module_init(module_start);
module_exit(module_stop);
